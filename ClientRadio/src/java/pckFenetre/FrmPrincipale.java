/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pckFenetre;

/**
 *
 * @author Etudiant
 */
public class FrmPrincipale extends javax.swing.JFrame {

    /**
     * Creates new form FrmPrincipale
     */
    public FrmPrincipale() {
        
        initComponents();
        
       
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFrame1 = new javax.swing.JFrame();
        jFrame2 = new javax.swing.JFrame();
        PanelHaut = new javax.swing.JPanel();
        PanelPatient = new javax.swing.JPanel();
        lbIdPatient = new javax.swing.JLabel();
        lbPatient = new javax.swing.JLabel();
        lbIdPatient1 = new javax.swing.JLabel();
        lbAllergie = new javax.swing.JLabel();
        lbTypeAllergie = new javax.swing.JLabel();
        PanelConnexion = new javax.swing.JPanel();
        lbNomConnecte = new javax.swing.JLabel();
        PanelGauche = new javax.swing.JPanel();
        PanelDroit = new javax.swing.JPanel();
        PanelBas = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        PanelCentre = new javax.swing.JPanel();
        MenuHaut = new javax.swing.JMenuBar();
        mniConnexion = new javax.swing.JMenu();
        mniDeconnecter = new javax.swing.JMenuItem();
        mniPatient = new javax.swing.JMenu();
        mniRecherPatient = new javax.swing.JMenuItem();
        mniAssocierPrescription = new javax.swing.JMenuItem();
        mniGererSubstance = new javax.swing.JMenuItem();
        mniAjouterExamen = new javax.swing.JMenu();
        mniRapportMedical = new javax.swing.JMenu();
        mniConsulter = new javax.swing.JMenuItem();
        mniRapportExamen = new javax.swing.JMenu();
        mniConsulterExamen = new javax.swing.JMenuItem();
        mniGestionSysteme = new javax.swing.JMenu();
        mniGerer = new javax.swing.JMenu();
        mniModelePatient = new javax.swing.JMenu();
        mniListePatient = new javax.swing.JMenuItem();
        mniReinitialiserModele = new javax.swing.JMenuItem();
        mniGererUtilisateur = new javax.swing.JMenuItem();
        mniRegistre = new javax.swing.JMenuItem();

        javax.swing.GroupLayout jFrame1Layout = new javax.swing.GroupLayout(jFrame1.getContentPane());
        jFrame1.getContentPane().setLayout(jFrame1Layout);
        jFrame1Layout.setHorizontalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jFrame1Layout.setVerticalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jFrame2Layout = new javax.swing.GroupLayout(jFrame2.getContentPane());
        jFrame2.getContentPane().setLayout(jFrame2Layout);
        jFrame2Layout.setHorizontalGroup(
            jFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jFrame2Layout.setVerticalGroup(
            jFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lbIdPatient.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        lbIdPatient.setText("royJ123");

        lbPatient.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lbPatient.setText("Patient");

        lbIdPatient1.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        lbIdPatient1.setText("Jonathan Roy");

        lbAllergie.setText("Allergie");

        lbTypeAllergie.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        lbTypeAllergie.setForeground(new java.awt.Color(255, 51, 51));
        lbTypeAllergie.setText("P�nicilline");

        javax.swing.GroupLayout PanelPatientLayout = new javax.swing.GroupLayout(PanelPatient);
        PanelPatient.setLayout(PanelPatientLayout);
        PanelPatientLayout.setHorizontalGroup(
            PanelPatientLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelPatientLayout.createSequentialGroup()
                .addGroup(PanelPatientLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelPatientLayout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(lbPatient))
                    .addGroup(PanelPatientLayout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addGroup(PanelPatientLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbAllergie)
                            .addComponent(lbIdPatient)
                            .addComponent(lbTypeAllergie))))
                .addContainerGap(133, Short.MAX_VALUE))
            .addGroup(PanelPatientLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(PanelPatientLayout.createSequentialGroup()
                    .addGap(24, 24, 24)
                    .addComponent(lbIdPatient1)
                    .addContainerGap(80, Short.MAX_VALUE)))
        );
        PanelPatientLayout.setVerticalGroup(
            PanelPatientLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelPatientLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(lbPatient)
                .addGap(41, 41, 41)
                .addComponent(lbIdPatient)
                .addGap(26, 26, 26)
                .addComponent(lbAllergie)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbTypeAllergie)
                .addContainerGap(32, Short.MAX_VALUE))
            .addGroup(PanelPatientLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(PanelPatientLayout.createSequentialGroup()
                    .addGap(47, 47, 47)
                    .addComponent(lbIdPatient1)
                    .addContainerGap(113, Short.MAX_VALUE)))
        );

        lbNomConnecte.setText("Francis Rioux est connect�");

        javax.swing.GroupLayout PanelConnexionLayout = new javax.swing.GroupLayout(PanelConnexion);
        PanelConnexion.setLayout(PanelConnexionLayout);
        PanelConnexionLayout.setHorizontalGroup(
            PanelConnexionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelConnexionLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbNomConnecte)
                .addContainerGap())
        );
        PanelConnexionLayout.setVerticalGroup(
            PanelConnexionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelConnexionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbNomConnecte)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout PanelHautLayout = new javax.swing.GroupLayout(PanelHaut);
        PanelHaut.setLayout(PanelHautLayout);
        PanelHautLayout.setHorizontalGroup(
            PanelHautLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelHautLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(PanelPatient, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 733, Short.MAX_VALUE)
                .addComponent(PanelConnexion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        PanelHautLayout.setVerticalGroup(
            PanelHautLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelHautLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PanelHautLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(PanelPatient, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(PanelConnexion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(PanelHaut, java.awt.BorderLayout.PAGE_START);

        javax.swing.GroupLayout PanelGaucheLayout = new javax.swing.GroupLayout(PanelGauche);
        PanelGauche.setLayout(PanelGaucheLayout);
        PanelGaucheLayout.setHorizontalGroup(
            PanelGaucheLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        PanelGaucheLayout.setVerticalGroup(
            PanelGaucheLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 66, Short.MAX_VALUE)
        );

        getContentPane().add(PanelGauche, java.awt.BorderLayout.LINE_START);

        javax.swing.GroupLayout PanelDroitLayout = new javax.swing.GroupLayout(PanelDroit);
        PanelDroit.setLayout(PanelDroitLayout);
        PanelDroitLayout.setHorizontalGroup(
            PanelDroitLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        PanelDroitLayout.setVerticalGroup(
            PanelDroitLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 66, Short.MAX_VALUE)
        );

        getContentPane().add(PanelDroit, java.awt.BorderLayout.LINE_END);

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jTextArea1.setText("Ceci est la zone qui est r�serv� aux diff�rentes boites de dialogue. ");
        jScrollPane2.setViewportView(jTextArea1);

        javax.swing.GroupLayout PanelBasLayout = new javax.swing.GroupLayout(PanelBas);
        PanelBas.setLayout(PanelBasLayout);
        PanelBasLayout.setHorizontalGroup(
            PanelBasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelBasLayout.createSequentialGroup()
                .addGap(105, 105, 105)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 974, Short.MAX_VALUE)
                .addGap(107, 107, 107))
        );
        PanelBasLayout.setVerticalGroup(
            PanelBasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelBasLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 511, Short.MAX_VALUE)
                .addGap(15, 15, 15))
        );

        getContentPane().add(PanelBas, java.awt.BorderLayout.PAGE_END);

        javax.swing.GroupLayout PanelCentreLayout = new javax.swing.GroupLayout(PanelCentre);
        PanelCentre.setLayout(PanelCentreLayout);
        PanelCentreLayout.setHorizontalGroup(
            PanelCentreLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 986, Short.MAX_VALUE)
        );
        PanelCentreLayout.setVerticalGroup(
            PanelCentreLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 66, Short.MAX_VALUE)
        );

        getContentPane().add(PanelCentre, java.awt.BorderLayout.CENTER);

        mniConnexion.setText("Connexion");

        mniDeconnecter.setText("D�connecter");
        mniConnexion.add(mniDeconnecter);

        MenuHaut.add(mniConnexion);

        mniPatient.setText("Patient");

        mniRecherPatient.setText("S�lectionner un patient");
        mniPatient.add(mniRecherPatient);

        mniAssocierPrescription.setText("Associer prescription");
        mniPatient.add(mniAssocierPrescription);

        mniGererSubstance.setText("G�rer substance administr�s");
        mniPatient.add(mniGererSubstance);

        MenuHaut.add(mniPatient);

        mniAjouterExamen.setText("+ Examen");
        MenuHaut.add(mniAjouterExamen);

        mniRapportMedical.setText("Rapport M�dical");

        mniConsulter.setText("Consulter");
        mniRapportMedical.add(mniConsulter);

        MenuHaut.add(mniRapportMedical);

        mniRapportExamen.setText("Rapport Examen");

        mniConsulterExamen.setText("Consulter rapport examen");
        mniRapportExamen.add(mniConsulterExamen);

        MenuHaut.add(mniRapportExamen);

        mniGestionSysteme.setText("Gestion Syst�me");

        mniGerer.setText("G�rer");

        mniModelePatient.setText("G�rer les mod�les patient");

        mniListePatient.setText("Liste des patients");
        mniModelePatient.add(mniListePatient);

        mniReinitialiserModele.setText("R�initialiser les mod�les patient");
        mniModelePatient.add(mniReinitialiserModele);

        mniGerer.add(mniModelePatient);

        mniGererUtilisateur.setText("G�rer les utilisateurs");
        mniGerer.add(mniGererUtilisateur);

        mniGestionSysteme.add(mniGerer);

        mniRegistre.setText("Registre");
        mniGestionSysteme.add(mniRegistre);

        MenuHaut.add(mniGestionSysteme);

        setJMenuBar(MenuHaut);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipale.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipale.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipale.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipale.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmPrincipale().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuBar MenuHaut;
    private javax.swing.JPanel PanelBas;
    private javax.swing.JPanel PanelCentre;
    private javax.swing.JPanel PanelConnexion;
    private javax.swing.JPanel PanelDroit;
    private javax.swing.JPanel PanelGauche;
    private javax.swing.JPanel PanelHaut;
    private javax.swing.JPanel PanelPatient;
    private javax.swing.JFrame jFrame1;
    private javax.swing.JFrame jFrame2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JLabel lbAllergie;
    private javax.swing.JLabel lbIdPatient;
    private javax.swing.JLabel lbIdPatient1;
    private javax.swing.JLabel lbNomConnecte;
    private javax.swing.JLabel lbPatient;
    private javax.swing.JLabel lbTypeAllergie;
    private javax.swing.JMenu mniAjouterExamen;
    private javax.swing.JMenuItem mniAssocierPrescription;
    private javax.swing.JMenu mniConnexion;
    private javax.swing.JMenuItem mniConsulter;
    private javax.swing.JMenuItem mniConsulterExamen;
    private javax.swing.JMenuItem mniDeconnecter;
    private javax.swing.JMenu mniGerer;
    private javax.swing.JMenuItem mniGererSubstance;
    private javax.swing.JMenuItem mniGererUtilisateur;
    private javax.swing.JMenu mniGestionSysteme;
    private javax.swing.JMenuItem mniListePatient;
    private javax.swing.JMenu mniModelePatient;
    private javax.swing.JMenu mniPatient;
    private javax.swing.JMenu mniRapportExamen;
    private javax.swing.JMenu mniRapportMedical;
    private javax.swing.JMenuItem mniRecherPatient;
    private javax.swing.JMenuItem mniRegistre;
    private javax.swing.JMenuItem mniReinitialiserModele;
    // End of variables declaration//GEN-END:variables
}
